=head1 NAME

Tails::Persistence::Role::SetupStep - role for persistence setup steps

=cut

package Tails::Persistence::Role::SetupStep;
use Moose::Role;
use MooseX::Types::Moose qw( :all );
use MooseX::Has::Sugar::Saccharin;



with 'Tails::Role::HasEncoding';
with 'Tails::Persistence::Role::HasStatusArea';

use 5.10.0;
use namespace::autoclean;
use autodie qw(:all);

use Glib qw{TRUE FALSE};
use Gtk3 qw{-init};

use Locale::gettext;
use POSIX;
setlocale(LC_MESSAGES, "");
textdomain("tails-persistence-setup");

requires '_build_main_box';
requires 'go_button_pressed';


=head1 ATTRIBUTES

=cut

has 'name' => required ro Str;

has 'main_box' => lazy_build rw 'Gtk3::VBox';

foreach (qw{title subtitle description}) {
    has $_ => lazy_build rw 'Gtk3::Label';
}

has 'go_button'   => lazy_build rw 'Gtk3::Button';
foreach (qw{go_callback success_callback}) {
    has $_ => required ro CodeRef;
}

foreach (qw{drive_vendor drive_model}) {
    has $_ => required ro Str;
}


=head1 CONSTRUCTORS

=cut

sub _build_title {
    my $self = shift;

    my $label = Gtk3::Label->new('');
    $label->set_alignment(0.0, 0.5);
    my $attrlist  = Pango::AttrList->new;
    $attrlist->insert($_)
        foreach ( Pango::AttrScale->new(1.3),Pango::AttrWeight->new('bold') );
    $label->set_attributes($attrlist);
    $label->set_padding(10, 10);

    return $label;
}

sub _build_subtitle {
    my $self = shift;

    my $label = Gtk3::Label->new('');
    $label->set_alignment(0.0, 0.5);
    my $attrlist  = Pango::AttrList->new;
    $attrlist->insert($_)
        foreach ( Pango::AttrScale->new(1.1),Pango::AttrWeight->new('bold') );
    $label->set_attributes($attrlist);
    $label->set_padding(10, 10);
    $label->set_line_wrap(TRUE);
    $label->set_line_wrap_mode('word');
    $label->set_single_line_mode(FALSE);

    return $label;
}

sub _build_description {
    my $self = shift;

    my $label = Gtk3::Label->new('');
    $label->set_alignment(0.0, 0.5);
    $label->set_padding(10, 10);
    $label->set_line_wrap(TRUE);
    $label->set_line_wrap_mode('word');
    $label->set_single_line_mode(FALSE);

    return $label;
}

sub _build_go_button {
    my $self = shift;

    my $button = Gtk3::Button->new;
    $button->set_sensitive(FALSE);
    $button->set_can_default(TRUE);
    $button->signal_connect('clicked', sub { $self->go_button_pressed });

    return $button;
}

no Moose::Role;
1;

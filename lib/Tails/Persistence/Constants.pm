package Tails::Persistence::Constants;
use Moose;
use MooseX::Has::Sugar::Saccharin;
use MooseX::Types::Moose qw{:all};
use MooseX::Types::Path::Class;

use autodie qw(:all);
use English qw{-no_match_vars};
use Method::Signatures::Simple;


=head1 Attributes

=cut
for (qw{partition_label partition_guid filesystem_type filesystem_label}) {
    has "persistence_$_" => lazy_build ro Str;
}

has "persistence_minimum_size"       => lazy_build ro Int;

has 'persistence_filesystem_options' => lazy_build ro 'HashRef[Str]';

has 'persistence_state_file'         =>
    isa        => 'Path::Class::File',
    is         => 'ro',
    lazy_build => 1,
    coerce     => 1,
    documentation => q{File where tails-greeter writes persistence state.};


=head1 Constructors

=cut
method _build_persistence_partition_label  { 'TailsData'  }
method _build_persistence_minimum_size     { 64 * 2 ** 20 }
method _build_persistence_filesystem_type  { 'ext4'       }
method _build_persistence_filesystem_label { 'TailsData'  }

method _build_persistence_filesystem_options {
    {
        label => $self->persistence_filesystem_label,
    };
}

method _build_persistence_partition_guid  {
    '8DA63339-0007-60C0-C436-083AC8230908' # Linux reserved
}

method _build_persistence_state_file { '/var/lib/live/config/tails.persistence' }

no Moose;
1; # End of Tails::Persistence::Constants

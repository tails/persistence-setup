=head1 NAME

Tails::Persistence::Step::Configure - configure which bits are persistent

=cut

package Tails::Persistence::Step::Configure;
use Moose;
use MooseX::Method::Signatures;
use MooseX::Types::Moose qw( :all );
use MooseX::Has::Sugar::Saccharin;



use Glib qw{TRUE FALSE};

use Carp::Assert::More;
use Number::Format qw(:subs);
use Tails::Persistence::Configuration::Button;
use Try::Tiny;

use Locale::gettext;
use POSIX;
setlocale(LC_MESSAGES, "");
textdomain("tails-persistence-setup");


=head1 ATTRIBUTES

=cut

has 'configuration' => required ro 'Tails::Persistence::Configuration';

has 'persistence_partition_device_file' => required ro Str;
has 'persistence_partition_size' => required ro Int;

has 'list_box' => lazy_build ro 'Gtk3::VBox';

has 'buttons'  =>
    lazy_build ro 'ArrayRef[Tails::Persistence::Configuration::Button]',
    traits  => [ 'Array' ],
    handles => {
        all_buttons => 'elements',
        push_button => 'push',
    };


=head1 CONSTRUCTORS

=cut

sub BUILD {
    my $self = shift;

    # Force initialization in the right order
    assert_defined($self->configuration);
    assert_defined($self->buttons);
    assert_defined($self->list_box);

    $self->title->set_text($self->encoding->decode(gettext(
        q{Persistence wizard - Persistent volume configuration}
    )));
    $self->subtitle->set_text($self->encoding->decode(gettext(
        q{Specify the files that will be saved in the persistent volume}
    )));
    $self->description->set_markup($self->encoding->decode(sprintf(
        # TRANSLATORS: partition, size, device vendor, device model
        gettext(q{The selected files will be stored in the encrypted partition %s (%s), on the <b>%s %s</b> device.}),
        $self->persistence_partition_device_file,
        format_bytes($self->persistence_partition_size, mode => "iec"),
        $self->drive_vendor,
        $self->drive_model
    )));
    $self->go_button->set_label($self->encoding->decode(gettext(q{Save})));
    $self->go_button->set_sensitive(TRUE);
}

method _build_main_box {
    my $box = Gtk3::VBox->new();
    $box->set_spacing(6);
    $box->pack_start($self->title, FALSE, FALSE, 0);
    $box->pack_start($self->subtitle, FALSE, FALSE, 0);
    $box->pack_start($self->description, FALSE, FALSE, 0);

    my $viewport = Gtk3::Viewport->new;
    $viewport->set_shadow_type('GTK_SHADOW_NONE');
    $viewport->add($self->list_box);
    my $scrolled_win = Gtk3::ScrolledWindow->new;
    $scrolled_win->set_policy('automatic', 'automatic');
    $scrolled_win->add($viewport);
    $box->pack_start($scrolled_win, TRUE, TRUE, 0);

    $box->pack_start($self->status_area, FALSE, FALSE, 0);

    my $button_alignment = Gtk3::Alignment->new(1.0, 0, 0.2, 1.0);
    $button_alignment->set_padding(0, 0, 10, 10);
    $button_alignment->add($self->go_button);
    $box->pack_start($button_alignment, FALSE, FALSE, 0);

    return $box;
}

method _build_buttons {
    [
        map {
            Tails::Persistence::Configuration::Button->new(atom => $_)
        } $self->configuration->all_atoms
    ]
}

method _build_list_box {
    my $box = Gtk3::VBox->new();
    $box->set_spacing(6);
    $box->pack_start($_->main_widget, FALSE, FALSE, 0) foreach $self->all_buttons;
    $self->refresh_buttons;

    return $box;
}


=head1 METHODS

=cut

method operation_finished ($error) {
    if ($error) {
        $self->working(0);
        say STDERR "$error";
        $self->subtitle->set_text($self->encoding->decode(gettext(q{Failed})));
        $self->description->set_text($error);
    }
    else {
        say STDERR "done.";
        $self->working(0);
        $self->success_callback->();
    }
}

method go_button_pressed {
    $self->list_box->hide;
    $self->working(1);
    $self->subtitle->set_text(
        $self->encoding->decode(gettext(q{Saving...})),
    );
    $self->description->set_text(
        $self->encoding->decode(gettext(q{Saving persistence configuration...})),
    );

    my $error;
    try {
        $self->go_callback->();
    } catch {
        $error = $@;
    };
    $self->operation_finished($error);
}

method refresh_buttons {
    $_->refresh_display foreach $self->all_buttons;
}

with 'Tails::Persistence::Role::StatusArea';
with 'Tails::Persistence::Role::SetupStep';

no Moose;
1;

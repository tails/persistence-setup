=head1 NAME

Tails::Persistence::Step::Bootstrap - bootstrap persistent storage

=cut

package Tails::Persistence::Step::Bootstrap;
use Moose;
use MooseX::Types::Moose qw( :all );
use MooseX::Has::Sugar::Saccharin;



use Glib qw{TRUE FALSE};

use IPC::System::Simple qw{systemx};
use Number::Format qw(:subs);

use Locale::gettext;
use POSIX;
setlocale(LC_MESSAGES, "");
textdomain("tails-persistence-setup");


=head1 ATTRIBUTES

=cut

foreach (qw{intro label verify_label warning_label}) {
    has $_ => lazy_build rw 'Gtk3::Label';
}

foreach (qw{passphrase_entry verify_passphrase_entry}) {
    has $_ => lazy_build rw 'Gtk3::Entry', builder '_build_passphrase_entry';
}

has 'table_alignment' => lazy_build rw 'Gtk3::Alignment';
has 'table'           => lazy_build rw 'Gtk3::Table';
has 'warning_area'    => lazy_build rw 'Gtk3::HBox';
has 'warning_image'   => lazy_build rw 'Gtk3::Image';
has 'size_of_free_space'      => required ro Int;
has 'mount_persistence_partition_cb' => required ro CodeRef;
has 'should_mount_persistence_partition' => required ro Bool;


=head1 CONSTRUCTORS

=cut

sub BUILD {
    my $self = shift;

    $self->title->set_text($self->encoding->decode(gettext(
        q{Persistence wizard - Persistent volume creation}
    )));
    $self->subtitle->set_text($self->encoding->decode(gettext(
        q{Choose a passphrase to protect the persistent volume}
    )));
    $self->description->set_markup($self->encoding->decode(sprintf(
        # TRANSLATORS: size, device vendor, device model
        gettext(q{A %s persistent volume will be created on the <b>%s %s</b> device. Data on this volume will be stored in an encrypted form protected by a passphrase.}),
        format_bytes($self->size_of_free_space, mode => "iec"),
        $self->drive_vendor,
        $self->drive_model,
    )));
    $self->go_button->set_label($self->encoding->decode(gettext(q{Create})));
}

sub _build_main_box {
    my $self = shift;


    my $box = Gtk3::VBox->new();
    $box->set_spacing(6);
    $box->pack_start($self->title, FALSE, FALSE, 0);
    $box->pack_start($self->intro, FALSE, FALSE, 0);
    $box->pack_start($self->subtitle, FALSE, FALSE, 0);
    $box->pack_start($self->description, FALSE, FALSE, 0);

    my $passphrase_box = Gtk3::VBox->new(FALSE, 6);
    $passphrase_box->set_spacing(6);
    $passphrase_box->pack_start($self->table_alignment, FALSE, FALSE, 0);
    $passphrase_box->pack_start($self->warning_area, FALSE, FALSE, 0);

    $box->pack_start($passphrase_box,  FALSE, FALSE, 0);

    $self->verify_passphrase_entry->set_activates_default(TRUE);

    $box->pack_start($self->status_area, FALSE, FALSE, 0);

    my $button_alignment = Gtk3::Alignment->new(1.0, 0, 0.2, 1.0);
    $button_alignment->set_padding(0, 0, 10, 10);
    $button_alignment->add($self->go_button);
    $box->pack_start($button_alignment, FALSE, FALSE, 0);
    $self->passphrase_entry->grab_focus();

    return $box;
}

sub _build_intro {
    my $self = shift;

    my $intro = Gtk3::Label->new('');
    $intro->set_alignment(0.0, 0.5);
    $intro->set_padding(10, 10);
    $intro->set_line_wrap(TRUE);
    $intro->set_line_wrap_mode('word');
    $intro->set_single_line_mode(FALSE);
    $intro->set_markup($self->encoding->decode(gettext(
        q{<b>Beware!</b> Using persistence has consequences that must be well understood. Tails can't help you if you use it wrong! See <a href='file:///usr/share/doc/tails/website/doc/first_steps/persistence.en.html'>Tails documentation about persistence</a> to learn more.}
    )));

    return $intro;
}

sub _build_warning_image {
    my $self = shift;

    Gtk3::Image->new_from_stock('gtk-dialog-info', 'menu');
}

sub _build_warning_area {
    my $self = shift;

    my $ext_hbox = Gtk3::HBox->new(FALSE, 0);
    $ext_hbox->set_border_width(10);
    $ext_hbox->set_spacing(6);

    my $box = Gtk3::HBox->new(FALSE, 12);
    $box->set_spacing(6);
    $box->pack_start($self->warning_image, FALSE, FALSE, 0);
    $box->pack_start($self->warning_label, FALSE, FALSE, 0);

    $ext_hbox->pack_start($box, FALSE, FALSE, 0);
    $ext_hbox->pack_start(Gtk3::Label->new(' '), FALSE, FALSE, 0);

    return $ext_hbox;
}

sub _build_label {
    my $self = shift;

    my $label = Gtk3::Label->new($self->encoding->decode(gettext(
        q{Passphrase:}
    )));
    $label->set_alignment(0.0, 0.5);
    return $label;
}

sub _build_verify_label {
    my $self = shift;

    my $label = Gtk3::Label->new($self->encoding->decode(gettext(
        q{Verify Passphrase:}
    )));
    $label->set_alignment(0.0, 0.5);
    return $label;
}

sub _build_warning_label {
    my $self = shift;

    my $label = Gtk3::Label->new('');
    $label->set_padding(10, 0);
    $label->set_markup(
          "<i>"
        . $self->encoding->decode(gettext(q{Passphrase can't be empty}))
        . "</i>"
    );
    return $label;
}

sub _build_passphrase_entry {
    my $self = shift;

    my $entry = Gtk3::Entry->new;
    $entry->set_visibility(FALSE);
    $entry->signal_connect("changed" => sub { $self->passphrase_entry_changed });

    return $entry;
}

sub _build_table_alignment {
    my $self = shift;

    my $table_alignment = Gtk3::Alignment->new(0.0, 0.0, 1.0, 1.0);
    $table_alignment->add($self->table);
    return $table_alignment;
}

sub _build_table {
    my $self = shift;

    my $table = Gtk3::Table->new(1, 2, FALSE);
    $table->set_border_width(10);
    $table->set_col_spacings(12);
    $table->set_row_spacings(6);
    $table->attach($self->label, 0, 1, 0, 1, 'fill', [qw{expand fill}], 0, 0);
    $table->attach_defaults($self->passphrase_entry, 1, 2, 0,  1);
    $table->attach($self->verify_label, 0, 1, 1, 2, 'fill', [qw{expand fill}], 0, 0);
    $table->attach_defaults($self->verify_passphrase_entry, 1, 2, 1,  2);

    return $table;
}


=head1 METHODS

=cut

sub update_passphrase_ui {
    my $self = shift;

    my $passphrase = $self->passphrase_entry->get_text;
    my $passphrase_verify = $self->verify_passphrase_entry->get_text;

    # TODO: check passphrase strength (#7002)

    if ($passphrase ne $passphrase_verify) {
        $self->warning_label->set_markup(
              "<i>"
            . $self->encoding->decode(gettext(q{Passphrases do not match}))
            . "</i>"
        );
        $self->warning_image->show;
        $self->go_button->set_sensitive(FALSE);
    }
    elsif (length($passphrase) == 0) {
        $self->warning_label->set_markup(
              "<i>"
            . $self->encoding->decode(gettext(q{Passphrase can't be empty}))
            . "</i>"
        );
        $self->warning_image->show;
        $self->go_button->set_sensitive(FALSE);
    }
    else {
        $self->warning_image->hide;
        $self->warning_label->set_text(' ');
        $self->go_button->set_sensitive(TRUE);
    }
}

sub passphrase_entry_changed {
    my $self = shift;

    $self->update_passphrase_ui;
}

sub operation_finished {
    my $self    = shift;
    my $replies = shift;
    my ($created_device, $error);

    say STDERR "Entering Bootstrap::operation_finished";

    if (exists($replies->{created_device}) && defined($replies->{created_device})) {
        $created_device = $replies->{created_device};
        # For some reason, we cannot get the exception when Try::Tiny is used,
        # so let's do it by hand.
        {
            local $@;
            eval { $replies->{format_reply}->get_result };
            $error = $@;
        }
    }
    else {
        $error = $replies->{create_partition_error};
    }

    if ($error) {
        $self->working(0);
        say STDERR "$error";
        $self->subtitle->set_text($self->encoding->decode(gettext(q{Failed})));
        $self->description->set_text($error);
    }
    else {
        say STDERR "created ${created_device}.";
        $self->working(0);

        if ($self->should_mount_persistence_partition) {
            $self->subtitle->set_text($self->encoding->decode(gettext(
                q{Mounting Tails persistence partition.}
            )));
            $self->description->set_text($self->encoding->decode(gettext(
                q{The Tails persistence partition will be mounted.}
            )));
            $self->working(1);
            systemx(qw{/sbin/udevadm settle});
            my $mountpoint = $self->mount_persistence_partition_cb->();
            $self->working(0);
            say STDERR "mounted persistence partition on $mountpoint";

            $self->subtitle->set_text($self->encoding->decode(gettext(
                q{Correcting permissions of the persistent volume.}
            )));
            $self->description->set_text($self->encoding->decode(gettext(
                q{The permissions of the persistent volume will be corrected.}
            )));
            $self->working(1);
            systemx(qw{sudo -n /usr/bin/tails-fix-persistent-volume-permissions});
            $self->working(0);
            say STDERR "fixed permissions.";

        }
        else {
            say STDERR "Should lock Luks device.";
        }

        $self->success_callback->();
    }
}

sub go_button_pressed {
    my $self = shift;

    $_->hide foreach ($self->intro, $self->warning_area, $self->table);
    $self->working(1);
    $self->subtitle->set_text(
        $self->encoding->decode(gettext(q{Creating...})),
    );
    $self->description->set_text(
        $self->encoding->decode(gettext(q{Creating the persistent volume...})),
    );

    $self->go_callback->(
        passphrase => $self->passphrase_entry->get_text,
        end_cb => sub { $self->operation_finished(@_) },
    );
}

with 'Tails::Persistence::Role::StatusArea';
with 'Tails::Persistence::Role::SetupStep';

no Moose;
1;

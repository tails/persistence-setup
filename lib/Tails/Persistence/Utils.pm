=head1 NAME

Tails::Persistence::Utils - utilities for Tails persistent storage

=cut

package Tails::Persistence::Utils;

use strict;
use warnings FATAL => 'all';
use 5.10.1;



use Exporter;
our @ISA = qw{Exporter};
our @EXPORT = qw{align_up_at_2MiB align_down_at_2MiB step_name_to_class_name get_variable_from_file check_config_file_permissions};

use Carp;
use IPC::System::Simple qw{capturex};
use List::MoreUtils qw{last_value};
use Path::Class;


=head1 FUNCTIONS

=cut

sub round_down {
    my $number = shift;
    my $round = shift;

    return (int($number/$round)) * $round if $number % $round;
    return $number;
}

sub round_up {
    my $number = shift;
    my $round = shift;

    return (1 + int($number/$round)) * $round if $number % $round;
    return $number;
}

sub align_up_at_2MiB {
    my $bytes = shift;

    round_up($bytes, 2 * 2 ** 20)
}

sub align_down_at_2MiB {
    my $bytes = shift;

    round_down($bytes, 2 * 2 ** 20)
}

sub step_name_to_class_name {
    my $name = shift;

    'Tails::Persistence::Step::' . ucfirst($name);
}

sub get_variable_from_file {
    my $file     = shift;
    my $variable = shift;

    foreach my $line (file($file)->slurp(chomp => 1)) {
        if (my ($name, $value) =
                ($line =~ m{\A [[:space:]]* ($variable)=(.*) \z}xms)) {
            return $value;
        }
    }

    return;
}

=head2 check_config_file_permissions

Refuse to read persistence.conf if it has different type, permission
or ownership than expected. We don't check the parent directory since
live-persist ensures it's right for us.

=cut
sub check_config_file_permissions {
    @_ == 2 or croak(q{check_config_file_permissions needs 2 arguments});
    my $config_file_path = shift;
    my $expected         = shift;

    croak(q{persistence.conf is a symbolic link})  if -l $config_file_path;
    croak(q{persistence.conf is not a plain file}) unless -f $config_file_path;

    my $st = $config_file_path->stat();

    # ownership
    foreach my $field (qw{uid gid}) {
        my $actual_value = $st->$field;
        $actual_value eq $expected->{$field}
            or croak("persistence.conf has unsafe $field: '$actual_value'");
    }

    # mode
    my $actual_mode   = sprintf("%04o",   $st->mode & oct(7777));
    my $expected_mode = sprintf("%04o", $expected->{mode});
    $actual_mode eq $expected_mode
        or croak(sprintf(
            "persistence.conf has unsafe mode: '%s'; expected: '%s'",
            $actual_mode, $expected_mode));

    # ACL
    capturex('/bin/getfacl', '--omit-header', '--skip-base', $config_file_path)
        eq $expected->{acl}
            or croak("persistence.conf has unsafe ACLs");

    return 1;
}

1;

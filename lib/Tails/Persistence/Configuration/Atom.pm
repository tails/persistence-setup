=head1 NAME

Tails::Persistence::Configuration::Atom - a GUI-friendly configuration line

=cut

package Tails::Persistence::Configuration::Atom;
use Moose;
use MooseX::Method::Signatures;
use MooseX::Types::Moose qw( :all );
use MooseX::Has::Sugar::Saccharin;



with 'Tails::Persistence::Role::ConfigurationLine';
with 'Tails::Role::HasEncoding';

use autodie qw(:all);
use warnings FATAL => 'all';

use List::MoreUtils qw{all pairwise};

use Locale::gettext;
use POSIX;
setlocale(LC_MESSAGES, "");
textdomain("tails-persistence-setup");


=head1 ATTRIBUTES

=cut

has 'enabled'     => required rw Bool;
has 'name'        => lazy_build rw Str;
has 'description' => lazy_build rw Str;
has 'icon_name'     => lazy_build rw Str;


=head1 CONSTRUCTORS

=cut

sub new_from_line {
    my $class = shift;
    my $line  = shift;
    my %args  = @_;

    Tails::Persistence::Configuration::Atom->new(
        destination => $line->destination,
        options     => $line->options,
        %args,
    );
}

method _build_name { $self->encoding->decode(gettext('Custom')); }

method _build_description {
    $self->encoding->decode($self->destination);
}

method _build_icon_name { 'dialog-question' }


=head1 METHODS

=cut


method equals_line ($line) {
    $self->destination eq $line->destination
        and
    $self->options_are($line->all_options);
}

sub options_are {
    my $self = shift;

    my @expected = sort(@_);
    my @options  = sort($self->all_options);

    return unless @expected == @options;

    all { $_ } pairwise {
        defined($a) and defined($b) and $a eq $b
    } @expected, @options;
}

no Moose;
1;

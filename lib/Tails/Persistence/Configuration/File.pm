=head1 NAME

Tails::Persistence::Configuration::File - read, parse and write live-persistence.conf

=cut

package Tails::Persistence::Configuration::File;
use Moose;
use MooseX::Method::Signatures;
use MooseX::Types::Moose qw( :all );
use MooseX::Types::Path::Class;
use MooseX::Has::Sugar::Saccharin;



use autodie qw(:all);
use warnings FATAL => 'all';
use Carp;
use Path::Class;
use Tails::Persistence::Configuration::Line;


=head1 ATTRIBUTES

=cut

has 'path' => (
    isa        => 'Path::Class::File',
    is         => 'ro',
    coerce     => 1,
    required   => 1,
);

has 'lines' =>
    lazy_build rw 'ArrayRef[Tails::Persistence::Configuration::Line]',
    traits  => [ 'Array' ],
    handles => {
        all_lines => 'elements',
    };


=head1 CONSTRUCTORS

=cut

sub BUILD {
    my $self = shift;

    $self->path->touch;
}

method _build_lines {
    return [
        grep { defined $_ } map {
            Tails::Persistence::Configuration::Line->new_from_string($_)
        } $self->path->slurp(chomp => 1)
    ];
}


=head1 METHODS

=cut

=head2 output

Returns the in-memory configuration, as a string in the live-persistence.conf format.

=cut
method output {
    my $out = "";
    foreach ($self->all_lines) {
        $out .= $_->stringify . "\n";
    }
    return $out;
}

=head2 save

Save the in-memory configuration to disk.
Throw exception on error.

=cut
method save {
    my $output = $self->output;
    my $fh = $self->path->openw();
    print $fh $output;
    $fh->sync;
    close $fh;
    chmod 0600, $self->path;
}

no Moose;
1;

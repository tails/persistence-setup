=head1 NAME

Tails::Persistence::Configuration::Presets - available configuration snippets

=cut

package Tails::Persistence::Configuration::Presets;
use Moose;
use MooseX::Method::Signatures;
use MooseX::Types::Moose qw( :all );
use MooseX::Has::Sugar::Saccharin;



with 'Tails::Role::HasEncoding';

use autodie qw(:all);
use warnings FATAL => 'all';
use Carp;
use Tails::Persistence::Configuration::Atom;

use Locale::gettext;
use POSIX;
setlocale(LC_MESSAGES, "");
textdomain("tails-persistence-setup");


=head1 ATTRIBUTES

=cut

has '_presets' =>
    lazy_build ro 'ArrayRef[Tails::Persistence::Configuration::Atom]',
    traits  => [ 'Array' ],
    handles => {
        count => 'count',
        all   => 'elements',
    };


=head1 CONSTRUCTORS

=cut

method _build__presets {
    my @presets = (
        {
            name        => $self->encoding->decode(gettext(q{Personal Data})),
            description => $self->encoding->decode(gettext(
                q{Keep files stored in the `Persistent' directory}
            )),
            destination => '/home/amnesia/Persistent',
            enabled     => 1,
            options     => [ 'source=Persistent' ],
            icon_name   => 'stock_folder',
        },
        {
            name        => $self->encoding->decode(gettext(q{GnuPG})),
            description => $self->encoding->decode(gettext(
                q{GnuPG keyrings and configuration}
            )),
            destination => '/home/amnesia/.gnupg',
            options     => [ 'source=gnupg' ],
            enabled     => 0,
            icon_name   => 'seahorse-key',
        },
        {
            name        => $self->encoding->decode(gettext(q{SSH Client})),
            description => $self->encoding->decode(gettext(
                q{SSH keys, configuration and known hosts}
            )),
            destination => '/home/amnesia/.ssh',
            options     => [ 'source=openssh-client'],
            enabled     => 0,
            icon_name   => 'seahorse-key-ssh',
        },
        {
            name        => $self->encoding->decode(gettext(q{Pidgin})),
            description => $self->encoding->decode(gettext(
                q{Pidgin profiles and OTR keyring}
            )),
            destination => '/home/amnesia/.purple',
            options     => [ 'source=pidgin' ],
            enabled     => 0,
            icon_name   => 'pidgin',
        },
        {
            name        => $self->encoding->decode(gettext(q{Thunderbird})),
            description => $self->encoding->decode(gettext(
                q{Thunderbird profiles and locally stored email}
            )),
            destination => '/home/amnesia/.thunderbird',
            options     => [ 'source=thunderbird' ],
            enabled     => 0,
            icon_name   => 'thunderbird',
        },
        {
            name        => $self->encoding->decode(gettext(q{GNOME Keyring})),
            description => $self->encoding->decode(gettext(
                q{Secrets stored by GNOME Keyring}
            )),
            destination => '/home/amnesia/.gnome2/keyrings',
            options     => [ 'source=gnome-keyrings' ],
            enabled     => 0,
            icon_name   => 'seahorse-key-personal',
        },
        {
            name        => $self->encoding->decode(gettext(q{Network Connections})),
            description => $self->encoding->decode(gettext(
                q{Configuration of network devices and connections}
            )),
            destination => '/etc/NetworkManager/system-connections',
            options     => [ 'source=nm-system-connections' ],
            enabled     => 0,
            icon_name   => 'network-wired',
        },
        {
            name        => $self->encoding->decode(gettext(q{Browser bookmarks})),
            description => $self->encoding->decode(gettext(
                q{Bookmarks saved in the Tor Browser}
            )),
            destination => '/home/amnesia/.mozilla/firefox/bookmarks',
            options     => [ 'source=bookmarks' ],
            enabled     => 0,
            icon_name   => 'user-bookmarks',
        },
        {
            name        => $self->encoding->decode(gettext(q{Printers})),
            description => $self->encoding->decode(gettext(
                q{Printers configuration}
            )),
            destination => '/etc/cups',
            options     => [ 'source=cups-configuration' ],
            enabled     => 0,
            icon_name   => 'printer',
        },
        {
            name        => $self->encoding->decode(gettext(q{Bitcoin client})),
            description => $self->encoding->decode(gettext(
                q{Electrum's bitcoin wallet and configuration}
            )),
            destination => '/home/amnesia/.electrum',
            options     => [ 'source=electrum' ],
            enabled     => 0,
            icon_name   => 'electrum',
        },
        {
            name        => $self->encoding->decode(gettext(q{APT Packages})),
            description => $self->encoding->decode(gettext(
                q{Packages downloaded by APT}
            )),
            destination => '/var/cache/apt/archives',
            options     => [ 'source=apt/cache' ],
            enabled     => 0,
            icon_name   => 'synaptic',
        },
        {
            name        => $self->encoding->decode(gettext(q{APT Lists})),
            description => $self->encoding->decode(gettext(
                q{Lists downloaded by APT}
            )),
            destination => '/var/lib/apt/lists',
            options     => [ 'source=apt/lists' ],
            enabled     => 0,
            icon_name   => 'synaptic',
        },
        {
            name        => $self->encoding->decode(gettext(q{Dotfiles})),
            description => $self->encoding->decode(gettext(
                q{Symlink into $HOME every file or directory found in the `dotfiles' directory}
            )),
            destination => '/home/amnesia',
            options     => [ 'source=dotfiles', 'link' ],
            enabled     => 0,
            icon_name   => 'preferences-desktop',
        },
    );

    return [
        map { Tails::Persistence::Configuration::Atom->new(%{$_}) } @presets
    ];
}


=head1 METHODS

=cut

sub set_state_from_lines {
    my $self = shift;
    my @lines = @_;

    foreach my $atom ($self->all) {
        $atom->enabled(1) if grep { $atom->equals_line($_) } @lines;
    }
}

no Moose;
1;

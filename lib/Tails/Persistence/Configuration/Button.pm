=head1 NAME

Tails::Persistence::Configuration::Button - a configuration button in the GUI

=cut

package Tails::Persistence::Configuration::Button;
use Moose;
use MooseX::Method::Signatures;
use MooseX::Types::Moose qw( :all );
use MooseX::Has::Sugar::Saccharin;



use autodie qw(:all);
use warnings FATAL => 'all';
use Glib qw{TRUE FALSE};
use Pango;


=head1 ATTRIBUTES

=cut

has 'atom'         => lazy_build ro 'Tails::Persistence::Configuration::Atom',
    handles => [
        qw{source destination all_options enabled name description icon_name}
    ];
has 'icon'         => lazy_build ro 'Gtk3::Image';
has 'checked_img'  => lazy_build ro 'Gtk3::Image';
has 'main_widget'  => lazy_build ro 'Gtk3::ToggleButton',
    handles => {
        is_active  => 'get_active',
        set_active => 'set_active',
    };
has 'icon_theme' => ro 'Gtk3::IconTheme', builder '_build_icon_theme';
has 'title_label'  => lazy_build ro 'Gtk3::Label';
has 'description_label' => lazy_build ro 'Gtk3::Label';


=head1 CONSTRUCTORS

=cut

method _build_icon {
    Gtk3::Image->new_from_pixbuf($self->icon_theme->load_icon(
        $self->icon_name, 48, 'use-builtin'
    ));
}

method _build_checked_img {
    # FIXME: we want the GTK_ICON_SIZE_SMALL_TOOLBAR
    # member of the GtkIconSize enum, not a magic "16" number here.
    Gtk3::Image->new_from_stock('gtk-apply', 16);
}

method _build_main_widget {
    my $line = shift;

    my $vbox = Gtk3::VBox->new();
    $vbox->set_border_width(5);
    $vbox->pack_start($self->title_label, FALSE, FALSE, 0);
    $vbox->pack_start($self->description_label, FALSE, FALSE, 0);

    my $hbox = Gtk3::HBox->new();
    $hbox->pack_start($self->icon, FALSE, FALSE, 0);
    $hbox->pack_start($vbox, TRUE, TRUE, 0);
    $hbox->pack_start($self->checked_img, FALSE, FALSE, 0);

    my $button = Gtk3::ToggleButton->new();
    $button->add($hbox);
    $button->set_active($self->enabled);
    $button->signal_connect('toggled' => sub { $self->toggled_cb });

    $button->signal_connect('show' => sub { $self->refresh_display });

    return $button;
}

method _build_icon_theme {
    my $theme = Gtk3::IconTheme::get_default();

    $theme->append_search_path('/usr/share/pixmaps/cryptui/48x48');
    $theme->append_search_path('/usr/share/pixmaps/seahorse/48x48');
    $theme->append_search_path('/usr/share/icons/gnome-colors-common/32x32/apps/');
    $theme->append_search_path('/usr/share/app-install/icons/');

    return $theme;
 }

method _build_title_label {
    my $title = Gtk3::Label->new($self->name);
    $title->set_alignment(0.0, 0.5);
    my $title_attrlist  = Pango::AttrList->new;
    $title_attrlist->insert($_)
        foreach ( Pango::AttrScale->new(1.1),Pango::AttrWeight->new('bold') );
    $title->set_attributes($title_attrlist);

    return $title;
}

method _build_description_label {
    my $description = Gtk3::Label->new($self->description);
    $description->set_alignment(0.0, 0.5);
    my $description_attrlist = Pango::AttrList->new;
    $description_attrlist->insert(
        Pango::AttrForeground->new(30000, 30000, 30000)
      );
    $description->set_attributes($description_attrlist);
    $description->set_line_wrap(TRUE);
    $description->set_line_wrap_mode('word');
    $description->set_single_line_mode(FALSE);

    return $description;
}


=head1 METHODS

=cut

method toggled_cb {
    $self->atom->enabled($self->is_active);
    $self->refresh_display;
}

method refresh_display {
    $self->refresh_checked_img;
    $self->refresh_text_color;
}

method refresh_checked_img {
    if ($self->enabled) {
        $self->checked_img->show;
    }
    else {
        $self->checked_img->hide;
    }
}

method refresh_text_color {
    my %color = $self->enabled ?
        (
            title       => 0,
            description => 30000,
    ) : (
            title       => 30000,
            description => 40000,
    );
    my $attrlist = $self->title_label->get_attributes;
    $attrlist->change(
        Pango::AttrForeground->new($color{title}, $color{title}, $color{title})
    );
    $self->title_label->set_attributes($attrlist);
    $attrlist = $self->description_label->get_attributes;
    $attrlist->change(
        Pango::AttrForeground->new($color{description}, $color{description}, $color{description})
    );
    $self->description_label->set_attributes($attrlist);
}

no Moose;
1;

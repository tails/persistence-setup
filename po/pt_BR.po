# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Tails developers
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Alexei Gonçalves de Oliveira <alexis@gessicahellmann.com>, 2018
# Communia <ameaneantie@riseup.net>, 2013-2018
# 8fcf29cd1b543ef9efd592d828027578, 2012
# carlo giusepe tadei valente sasaki <carlo.gt.valente@gmail.com>, 2014
# Communia <ameaneantie@riseup.net>, 2013
# Eduardo Addad de Oliveira <eduardoaddad@hotmail.com>, 2018
# Eduardo Bonsi, 2013
# 8fcf29cd1b543ef9efd592d828027578, 2012
# Lucas Possatti, 2014
# Malkon F <malkon.inf@gmail.com>, 2018
# Rafael Martins <rafaelmartinsrm@gmail.com>, 2012
# Nicole calderari <nicole_calderari@hotmail.com>, 2013
# Onthefrontline Web <develop@onthefrontline.com.br>, 2016
# radamantys seh <radamantys158@gmail.com>, 2013
# radamantys seh <radamantys158@gmail.com>, 2013
# Rafael Martins <rafaelmartinsrm@gmail.com>, 2012
# Reurison Silva Rodrigues, 2018
msgid ""
msgstr ""
"Project-Id-Version: Tor Project\n"
"Report-Msgid-Bugs-To: Tails developers <tails@boum.org>\n"
"POT-Creation-Date: 2019-10-21 09:06+0200\n"
"PO-Revision-Date: 2019-01-15 12:21+0000\n"
"Last-Translator: Communia <ameaneantie@riseup.net>\n"
"Language-Team: Portuguese (Brazil) (http://www.transifex.com/otf/torproject/"
"language/pt_BR/)\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../lib/Tails/Persistence/Setup.pm:265
msgid "Setup Tails persistent volume"
msgstr "Configurar o volume persistente do Tails"

#: ../lib/Tails/Persistence/Setup.pm:343 ../lib/Tails/Persistence/Setup.pm:481
msgid "Error"
msgstr "Erro"

#: ../lib/Tails/Persistence/Setup.pm:372
#, perl-format
msgid "Device %s already has a persistent volume."
msgstr "O dispositivo %s já possui um volume persistente."

#: ../lib/Tails/Persistence/Setup.pm:380
#, perl-format
msgid "Device %s has not enough unallocated space."
msgstr "O dispositivo %s não possui espaço livre suficiente."

#: ../lib/Tails/Persistence/Setup.pm:387 ../lib/Tails/Persistence/Setup.pm:401
#, perl-format
msgid "Device %s has no persistent volume."
msgstr "O dispositivo %s não possui nenhum volume persistente."

#: ../lib/Tails/Persistence/Setup.pm:393
#, perl-format
msgid ""
"Cannot delete the persistent volume on %s while in use. You should restart "
"Tails without persistence."
msgstr ""
"Não é possível deletar o volume persistente em %s durante o seu uso. Você "
"deve reiniciar o Tails sem habilitar o armazenamento persistente."

#: ../lib/Tails/Persistence/Setup.pm:407
#, perl-format
msgid "Persistence volume on %s is not unlocked."
msgstr "O volume persistente em %s não está desbloqueado."

#: ../lib/Tails/Persistence/Setup.pm:412
#, perl-format
msgid "Persistence volume on %s is not mounted."
msgstr "O volume persistente em %s não está montado."

#: ../lib/Tails/Persistence/Setup.pm:417
#, perl-format
msgid ""
"Persistence volume on %s is not readable. Permissions or ownership problems?"
msgstr ""
"Não é possível ler o volume persistente em %s. Problemas com permissões ou "
"propriedades?"

#: ../lib/Tails/Persistence/Setup.pm:422
#, perl-format
msgid "Persistence volume on %s is not writable."
msgstr "Não é possível escrever no volume persistente em %s."

#: ../lib/Tails/Persistence/Setup.pm:431
#, perl-format
msgid "Tails is running from non-USB / non-SDIO device %s."
msgstr ""
"O Tails está sendo executado em um dispositivo que não é USB /SDIO  %s."

#: ../lib/Tails/Persistence/Setup.pm:437
#, perl-format
msgid "Device %s is optical."
msgstr "%s é um dispositivo óptico."

#: ../lib/Tails/Persistence/Setup.pm:444
#, fuzzy, perl-format
msgid "Device %s was not created using a USB image or Tails Installer."
msgstr "O dispositivo %s não foi criado usando o Instalador Tails."

#: ../lib/Tails/Persistence/Setup.pm:688
msgid "Persistence wizard - Finished"
msgstr "Assistente do volume persistente - Terminou"

#: ../lib/Tails/Persistence/Setup.pm:691
msgid ""
"Any changes you have made will only take effect after restarting Tails.\n"
"\n"
"You may now close this application."
msgstr ""
"Todas as alterações realizadas terão efeito somente após a reinicialização "
"do Tails.\n"
"\n"
"Você já pode fechar esta aplicação."

#: ../lib/Tails/Persistence/Configuration/Setting.pm:113
msgid "Custom"
msgstr "Personalizado"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:55
msgid "Personal Data"
msgstr "Dados Pessoais"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:57
msgid "Keep files stored in the `Persistent' directory"
msgstr "Manter os arquivos armazenados no diretório 'Persistent'"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:70
msgid "Browser Bookmarks"
msgstr "Favoritos do Navegador"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:72
msgid "Bookmarks saved in the Tor Browser"
msgstr "Favoritos salvos no navegador Tor"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:85
msgid "Network Connections"
msgstr "Conexões de Rede"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:87
msgid "Configuration of network devices and connections"
msgstr "Configuração de dispositivos e conexões de rede"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:100
msgid "Additional Software"
msgstr "Software Adicional"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:102
msgid "Software installed when starting Tails"
msgstr "Software instalado ao iniciar o Tails"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:120
msgid "Printers"
msgstr "Impressoras"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:122
msgid "Printers configuration"
msgstr "Configuração das impressoras"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:135
msgid "Thunderbird"
msgstr "Thunderbird"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:137
msgid "Thunderbird emails, feeds, and settings"
msgstr "Thunderbird e-mails, notificações e configurações"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:150
msgid "GnuPG"
msgstr "GnuPG (Gnu Privacy Guard)"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:152
msgid "GnuPG keyrings and configuration"
msgstr "Chaveiros e configuração do GnuPG"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:165
msgid "Bitcoin Client"
msgstr "Cliente Bitcoin"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:167
msgid "Electrum's bitcoin wallet and configuration"
msgstr "Configuração et carteira de bitcoin Electrum"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:180
msgid "Pidgin"
msgstr "Pidgin"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:182
msgid "Pidgin profiles and OTR keyring"
msgstr "Perfis do Pidgin e chaveiro OTR (Off-The-Record)"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:195
msgid "SSH Client"
msgstr "Cliente SSH (Secure SHell)"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:197
msgid "SSH keys, configuration and known hosts"
msgstr "Chaves SSH, configurações e servidores conhecidos"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:210
msgid "Dotfiles"
msgstr "Arquivos de configuração (Dotfiles)"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:212
msgid ""
"Symlink into $HOME every file or directory found in the `dotfiles' directory"
msgstr ""
"Criar um link em $HOME para todos os arquivos ou diretórios encontrados no "
"diretório `dotfiles`"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:96
msgid "Persistence wizard - Persistent volume creation"
msgstr "Assistente do volume persistente - Criação do volume persistente"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:99
msgid "Choose a passphrase to protect the persistent volume"
msgstr "Escolha uma frase-senha para proteger o volume Persistent."

#. TRANSLATORS: size, device vendor, device model
#: ../lib/Tails/Persistence/Step/Bootstrap.pm:103
#, perl-format
msgid ""
"A %s persistent volume will be created on the <b>%s %s</b> device. Data on "
"this volume will be stored in an encrypted form protected by a passphrase."
msgstr ""
"Um %s volume Persistent será criado no dispositivo <b>%s %s</b>. Os dados "
"deste dispositivo serão armazenados de forma criptografada e protegidos por "
"uma frase-senha."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:108
msgid "Create"
msgstr "Criar"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:151
msgid ""
"<b>Beware!</b> Using persistence has consequences that must be well "
"understood. Tails can't help you if you use it wrong! See the <i>Encrypted "
"persistence</i> page of the Tails documentation to learn more."
msgstr ""
"<b>Cuidado!</b> O uso de persistência tem consequências que precisam ser bem "
"compreendidas. O Tails não poderá ajudá-lo caso você o use incorretamente! "
"Consulte a página sobre <i>persistência criptografada</i> na documentação do "
"Tails para saber mais."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:179
msgid "Passphrase:"
msgstr "Frase-senha:"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:187
msgid "Verify Passphrase:"
msgstr "Verifique a frase-senha:"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:198
#: ../lib/Tails/Persistence/Step/Bootstrap.pm:266
msgid "Passphrase can't be empty"
msgstr "A frase-senha não pode ser vazia"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:233
#, fuzzy
msgid "Show Passphrase"
msgstr "Frase-senha:"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:257
msgid "Passphrases do not match"
msgstr "As frases-senha não correspondem"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:312
#: ../lib/Tails/Persistence/Step/Delete.pm:103
#: ../lib/Tails/Persistence/Step/Configure.pm:181
msgid "Failed"
msgstr "Falhou"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:320
msgid "Mounting Tails persistence partition."
msgstr "Montando a partição do volume persistente do Tails."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:323
msgid "The Tails persistence partition will be mounted."
msgstr "A partição do volume persistente do Tails será montada."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:332
msgid "Correcting permissions of the persistent volume."
msgstr "Corrigindo as permissões do volume persistente."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:335
msgid "The permissions of the persistent volume will be corrected."
msgstr "As permissões do volume persistente serão corrigidas."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:343
msgid "Creating default persistence configuration."
msgstr "Criando a configuração permanente padrão."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:346
msgid "The default persistence configuration will be created."
msgstr "A configuração permanente padrão vai ser criada."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:361
msgid "Creating..."
msgstr "Criando..."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:364
msgid "Creating the persistent volume..."
msgstr "Criando o volume persistente..."

#: ../lib/Tails/Persistence/Step/Delete.pm:53
msgid "Persistence wizard - Persistent volume deletion"
msgstr "Assistente do volume persistente - Supressão do volume persistente"

#: ../lib/Tails/Persistence/Step/Delete.pm:56
msgid "Your persistent data will be deleted."
msgstr "Seus dados persistentes serão suprimidos."

#: ../lib/Tails/Persistence/Step/Delete.pm:60
#, perl-format
msgid ""
"The persistent volume %s (%s), on the <b>%s %s</b> device, will be deleted."
msgstr ""
"O volume persistente %s (%s), no dispositivo <b>%s %s</b>, será suprimido."

#: ../lib/Tails/Persistence/Step/Delete.pm:66
msgid "Delete"
msgstr "Suprimir"

#: ../lib/Tails/Persistence/Step/Delete.pm:117
msgid "Deleting..."
msgstr "Suprimindo..."

#: ../lib/Tails/Persistence/Step/Delete.pm:120
msgid "Deleting the persistent volume..."
msgstr "Suprimindo o volume persistente..."

#: ../lib/Tails/Persistence/Step/Configure.pm:88
msgid "Persistence wizard - Persistent volume configuration"
msgstr "Assistente do volume persistente - Configuração do volume persistente"

#: ../lib/Tails/Persistence/Step/Configure.pm:91
msgid "Specify the files that will be saved in the persistent volume"
msgstr "Especifique os arquivos que serão salvos no volume persistente"

#. TRANSLATORS: partition, size, device vendor, device model
#: ../lib/Tails/Persistence/Step/Configure.pm:95
#, perl-format
msgid ""
"The selected files will be stored in the encrypted partition %s (%s), on the "
"<b>%s %s</b> device."
msgstr ""
"Os arquivos selecionados serão armazenados na partição criptografada %s "
"(%s), no dispositivo <b>%s %s</b>."

#: ../lib/Tails/Persistence/Step/Configure.pm:101
msgid "Save"
msgstr "Salvar"

#: ../lib/Tails/Persistence/Step/Configure.pm:195
msgid "Saving..."
msgstr "Salvando..."

#: ../lib/Tails/Persistence/Step/Configure.pm:198
msgid "Saving persistence configuration..."
msgstr "Salvando a configuração do volume persistente..."

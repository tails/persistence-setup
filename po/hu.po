# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Tails developers
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# benewfy <benewfy@gmail.com>, 2015
# Zoltán Faludi <info@falu.me>, 2016-2017
# Blackywantscookies, 2014
# Lajos Pasztor <mrlajos@gmail.com>, 2014
# cicus, 2014
# vargaviktor <viktor.varga@gmail.com>, 2013,2015,2018
# vargaviktor <viktor.varga@gmail.com>, 2012
msgid ""
msgstr ""
"Project-Id-Version: Tor Project\n"
"Report-Msgid-Bugs-To: Tails developers <tails@boum.org>\n"
"POT-Creation-Date: 2019-10-21 09:06+0200\n"
"PO-Revision-Date: 2018-12-28 14:53+0000\n"
"Last-Translator: vargaviktor <viktor.varga@gmail.com>\n"
"Language-Team: Hungarian (http://www.transifex.com/otf/torproject/language/"
"hu/)\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../lib/Tails/Persistence/Setup.pm:265
msgid "Setup Tails persistent volume"
msgstr "Tails tartós kötet telepítése"

#: ../lib/Tails/Persistence/Setup.pm:343 ../lib/Tails/Persistence/Setup.pm:481
msgid "Error"
msgstr "Hiba"

#: ../lib/Tails/Persistence/Setup.pm:372
#, perl-format
msgid "Device %s already has a persistent volume."
msgstr "A(z) %s eszköz már rendelkezik tartós kötettel."

#: ../lib/Tails/Persistence/Setup.pm:380
#, perl-format
msgid "Device %s has not enough unallocated space."
msgstr "A(z) %s eszköz nem rendelkezik elég lefoglalatlan szabad hellyel."

#: ../lib/Tails/Persistence/Setup.pm:387 ../lib/Tails/Persistence/Setup.pm:401
#, perl-format
msgid "Device %s has no persistent volume."
msgstr "A(z) %s eszköz nem tartalmaz tartós kötetet."

#: ../lib/Tails/Persistence/Setup.pm:393
#, perl-format
msgid ""
"Cannot delete the persistent volume on %s while in use. You should restart "
"Tails without persistence."
msgstr ""
"Nem törölhető a tartós kötet a(z)%s-en, amíg használatban van. Újra jell "
"indítania a Tails-t tartóstár nélkül."

#: ../lib/Tails/Persistence/Setup.pm:407
#, perl-format
msgid "Persistence volume on %s is not unlocked."
msgstr "A tartós kötet a(z) %s-en nincs feloldva."

#: ../lib/Tails/Persistence/Setup.pm:412
#, perl-format
msgid "Persistence volume on %s is not mounted."
msgstr "A tartós kötet a(z)%s -en nincs csatolva."

#: ../lib/Tails/Persistence/Setup.pm:417
#, perl-format
msgid ""
"Persistence volume on %s is not readable. Permissions or ownership problems?"
msgstr ""
"A tartós kötet a(z) %s-en nem olvasható. Jogosultság vagy tulajdonosi "
"probléma?"

#: ../lib/Tails/Persistence/Setup.pm:422
#, perl-format
msgid "Persistence volume on %s is not writable."
msgstr "A tartós kötet a(z) %s-en nem írható."

#: ../lib/Tails/Persistence/Setup.pm:431
#, perl-format
msgid "Tails is running from non-USB / non-SDIO device %s."
msgstr "Tails nem USB vagy SDIO eszközről %s fut."

#: ../lib/Tails/Persistence/Setup.pm:437
#, perl-format
msgid "Device %s is optical."
msgstr "A(z)  %s eszköz optikai."

#: ../lib/Tails/Persistence/Setup.pm:444
#, fuzzy, perl-format
msgid "Device %s was not created using a USB image or Tails Installer."
msgstr "Az eszközöd (%s) nem a Tails telepítővel volt létrehozva."

#: ../lib/Tails/Persistence/Setup.pm:688
msgid "Persistence wizard - Finished"
msgstr "Tartóstár varázsló - Befejezve"

#: ../lib/Tails/Persistence/Setup.pm:691
msgid ""
"Any changes you have made will only take effect after restarting Tails.\n"
"\n"
"You may now close this application."
msgstr ""
"Bármilyen változtatás történt csak a Tails újraindítása után lép érvénybe.\n"
"\n"
"Bezárhatja az alkalmazást."

#: ../lib/Tails/Persistence/Configuration/Setting.pm:113
msgid "Custom"
msgstr "Egyéni"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:55
msgid "Personal Data"
msgstr "Személyes adat"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:57
msgid "Keep files stored in the `Persistent' directory"
msgstr "A fájlok tárolása a 'Tartós' könyvtárban"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:70
msgid "Browser Bookmarks"
msgstr "Böngésző könyvjelzők"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:72
msgid "Bookmarks saved in the Tor Browser"
msgstr "Könyvjelzők elmentve a Tor Browser-ben."

#: ../lib/Tails/Persistence/Configuration/Presets.pm:85
msgid "Network Connections"
msgstr "Hálózati kapcsolatok"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:87
msgid "Configuration of network devices and connections"
msgstr "Hálózati eszközök és kapcsolatok beállítása"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:100
msgid "Additional Software"
msgstr "További szoftverek"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:102
msgid "Software installed when starting Tails"
msgstr "A Tails indulásakor telepített szoftverek"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:120
msgid "Printers"
msgstr "Nyomtatók"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:122
msgid "Printers configuration"
msgstr "Nyomtatók konfigurálása"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:135
msgid "Thunderbird"
msgstr "Thunderbird"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:137
msgid "Thunderbird emails, feeds, and settings"
msgstr "Thunderbird emailek, feedek és beállítások"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:150
msgid "GnuPG"
msgstr "GnuPG"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:152
msgid "GnuPG keyrings and configuration"
msgstr "GnuPG kulcstárolók és konfiguráció"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:165
msgid "Bitcoin Client"
msgstr "Bitcoin kliens"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:167
msgid "Electrum's bitcoin wallet and configuration"
msgstr "Electum bitcoin tárca és konfiguráció"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:180
msgid "Pidgin"
msgstr "Pidgin"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:182
msgid "Pidgin profiles and OTR keyring"
msgstr "Pidgin profilok és OTR kulcstár"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:195
msgid "SSH Client"
msgstr "SSH kliens"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:197
msgid "SSH keys, configuration and known hosts"
msgstr "SSH kulcsok, konfiguráció és ismert számítógépek"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:210
msgid "Dotfiles"
msgstr "Dot-fájlok"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:212
msgid ""
"Symlink into $HOME every file or directory found in the `dotfiles' directory"
msgstr ""
"Symlink a $HOME könyvtárban minden fájlra és könyvtárra található a "
"`dotfiles' könyvtárban"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:96
msgid "Persistence wizard - Persistent volume creation"
msgstr "Tartóstár varázsló - Tartós kötet létrehozása"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:99
msgid "Choose a passphrase to protect the persistent volume"
msgstr "Válassz jelszót a tartós kötet védelméhez."

#. TRANSLATORS: size, device vendor, device model
#: ../lib/Tails/Persistence/Step/Bootstrap.pm:103
#, perl-format
msgid ""
"A %s persistent volume will be created on the <b>%s %s</b> device. Data on "
"this volume will be stored in an encrypted form protected by a passphrase."
msgstr ""
"A %s tartós kötet létre lesz hozva itt <b>%s %s</b> eszközön. Az adatok "
"titkosított formában lesznek tárolva egy jelszóval."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:108
msgid "Create"
msgstr "Létrehoz"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:151
msgid ""
"<b>Beware!</b> Using persistence has consequences that must be well "
"understood. Tails can't help you if you use it wrong! See the <i>Encrypted "
"persistence</i> page of the Tails documentation to learn more."
msgstr ""
"<b>Figyelem</b> A tartóstár használata olyan következményekhez vezethet, "
"amit jól meg kell érteni. A Tails nem tud segíteni azon, ha rosszul "
"csinálja! Tekintse meg a <i>Titkosított tartóstár</i> oldalt a Tails "
"dokumentációban a részletekért."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:179
msgid "Passphrase:"
msgstr "Jelszó:"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:187
msgid "Verify Passphrase:"
msgstr "Jelszó ellenőrzése:"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:198
#: ../lib/Tails/Persistence/Step/Bootstrap.pm:266
msgid "Passphrase can't be empty"
msgstr "A jelszó nem lehet üres"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:233
#, fuzzy
msgid "Show Passphrase"
msgstr "Jelszó:"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:257
msgid "Passphrases do not match"
msgstr "A jelszavak nem egyeznek"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:312
#: ../lib/Tails/Persistence/Step/Delete.pm:103
#: ../lib/Tails/Persistence/Step/Configure.pm:181
msgid "Failed"
msgstr "Sikertelen"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:320
msgid "Mounting Tails persistence partition."
msgstr "Tails tartós partíció csatolása."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:323
msgid "The Tails persistence partition will be mounted."
msgstr "A Tails tartós partíció csatolva lesz."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:332
msgid "Correcting permissions of the persistent volume."
msgstr "Hozzáférési jogosultságok javítása a tartós köteten."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:335
msgid "The permissions of the persistent volume will be corrected."
msgstr "A hozzáférési jogosultságok javítva lettek a tartós köteten."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:343
msgid "Creating default persistence configuration."
msgstr "Alapértelmezett tartóstár konfiguráció létrehozása"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:346
msgid "The default persistence configuration will be created."
msgstr "Az alapértelmezett tartóstár konfiguráció kerül létrehozásra."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:361
msgid "Creating..."
msgstr "Létrehozás..."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:364
msgid "Creating the persistent volume..."
msgstr "Tartós kötet létrehozása..."

#: ../lib/Tails/Persistence/Step/Delete.pm:53
msgid "Persistence wizard - Persistent volume deletion"
msgstr "Tartóstár varázsló - Tartós kötet törlés"

#: ../lib/Tails/Persistence/Step/Delete.pm:56
msgid "Your persistent data will be deleted."
msgstr "Az tartós adatok törlésre kerülnek."

#: ../lib/Tails/Persistence/Step/Delete.pm:60
#, perl-format
msgid ""
"The persistent volume %s (%s), on the <b>%s %s</b> device, will be deleted."
msgstr "Az tartós kötet %s (%s), itt <b>%s %s</b> eszközön törölve lesz."

#: ../lib/Tails/Persistence/Step/Delete.pm:66
msgid "Delete"
msgstr "Törlés"

#: ../lib/Tails/Persistence/Step/Delete.pm:117
msgid "Deleting..."
msgstr "Törlés..."

#: ../lib/Tails/Persistence/Step/Delete.pm:120
msgid "Deleting the persistent volume..."
msgstr "A tartós kötet törlése..."

#: ../lib/Tails/Persistence/Step/Configure.pm:88
msgid "Persistence wizard - Persistent volume configuration"
msgstr "Tartóstár varázsló - Tartós kötet konfigurálása"

#: ../lib/Tails/Persistence/Step/Configure.pm:91
msgid "Specify the files that will be saved in the persistent volume"
msgstr "Határozd meg azokat a fájlokat amik az tartós kötetbe lesznek mentve"

#. TRANSLATORS: partition, size, device vendor, device model
#: ../lib/Tails/Persistence/Step/Configure.pm:95
#, perl-format
msgid ""
"The selected files will be stored in the encrypted partition %s (%s), on the "
"<b>%s %s</b> device."
msgstr ""
"A kiválasztott fájlok tárolva lesznek a titkosított partíción %s (%s), itt "
"<b>%s %s</b> eszközön."

#: ../lib/Tails/Persistence/Step/Configure.pm:101
msgid "Save"
msgstr "Mentés"

#: ../lib/Tails/Persistence/Step/Configure.pm:195
msgid "Saving..."
msgstr "Mentés..."

#: ../lib/Tails/Persistence/Step/Configure.pm:198
msgid "Saving persistence configuration..."
msgstr "Tartóstár beállítások mentése..."

# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Tails developers
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# ganeshwaki <ganeshwaki@gmail.com>, 2013
# Sachie <jemabha@gmail.com>, 2013
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: Tails developers <tails@boum.org>\n"
"POT-Creation-Date: 2017-05-15 13:51+0200\n"
"PO-Revision-Date: 2013-08-02 08:38+0000\n"
"Last-Translator: runasand <runa.sandvik@gmail.com>\n"
"Language-Team: Sinhala (Sri Lanka) (http://www.transifex.com/projects/p/"
"torproject/language/si_LK/)\n"
"Language: si_LK\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:48
msgid "Personal Data"
msgstr "පෞද්ගලික දත්ත"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:50
msgid "Keep files stored in the `Persistent' directory"
msgstr "'ස්ථාවර' කියන නාමාවලියේ ගොනු ගබඩා කර තබන්න"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:58
msgid "GnuPG"
msgstr "GnuPG"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:60
msgid "GnuPG keyrings and configuration"
msgstr "GnuPG keyrings සහ වින්‍යාසය"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:68
msgid "SSH Client"
msgstr "SSH දායකයා"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:70
msgid "SSH keys, configuration and known hosts"
msgstr "SSH යතුරු, වින්‍යාස සහ දන්නා සත්කාරක"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:78
msgid "Pidgin"
msgstr "Pidgin"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:80
msgid "Pidgin profiles and OTR keyring"
msgstr "Pidgin පැතිකඩ සහ OTR keyring"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:88
msgid "Thunderbird"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:90
#, fuzzy
msgid "Thunderbird profiles and locally stored email"
msgstr "Claws තැපෑල පැතිකඩ ස්ථානීය තැන්පත් කල විද්‍යුත් තැපෑල "

#: ../lib/Tails/Persistence/Configuration/Presets.pm:98
msgid "GNOME Keyring"
msgstr "GNOME Keyring"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:100
msgid "Secrets stored by GNOME Keyring"
msgstr "GNOME Keyring විසින් තැන්පත් කල රහස්"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:108
msgid "Network Connections"
msgstr "ජාල සබදතා"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:110
msgid "Configuration of network devices and connections"
msgstr "ජාල උපාංග සහ සබදතා වින්‍යාසය "

#: ../lib/Tails/Persistence/Configuration/Presets.pm:118
msgid "Browser bookmarks"
msgstr "බ්‍රව්සර පිටුසටහන් "

#: ../lib/Tails/Persistence/Configuration/Presets.pm:120
#, fuzzy
msgid "Bookmarks saved in the Tor Browser"
msgstr "පිටුසටහන් Iceweasel බ්‍රව්සරයේ සුරක්ෂිත කෙරේ."

#: ../lib/Tails/Persistence/Configuration/Presets.pm:128
msgid "Printers"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:130
#, fuzzy
msgid "Printers configuration"
msgstr "GnuPG keyrings සහ වින්‍යාසය"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:138
msgid "Bitcoin client"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:140
msgid "Electrum's bitcoin wallet and configuration"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:148
msgid "APT Packages"
msgstr "APT පැකේජ"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:150
msgid "Packages downloaded by APT"
msgstr "APT විසින් භාගත කරන ලද පැකේජ "

#: ../lib/Tails/Persistence/Configuration/Presets.pm:158
msgid "APT Lists"
msgstr "APT ලැයිස්තු"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:160
msgid "Lists downloaded by APT"
msgstr "APT විසින් භාගත කරන ලද ලැයිස්තු"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:168
msgid "Dotfiles"
msgstr "ඩොට්ගොනු"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:170
msgid ""
"Symlink into $HOME every file or directory found in the `dotfiles' directory"
msgstr "`dotfiles' නාමාවලියේ ඇති සියළු ගොනු හා නාමාවලි  $HOME වලට symlink කරන්න."

#: ../lib/Tails/Persistence/Setup.pm:230
msgid "Setup Tails persistent volume"
msgstr "Tails ස්ථාවර වෙළුම පිහිටුවන්න."

#: ../lib/Tails/Persistence/Setup.pm:312 ../lib/Tails/Persistence/Setup.pm:459
msgid "Error"
msgstr "දෝෂයකි"

#: ../lib/Tails/Persistence/Setup.pm:344
#, perl-format
msgid "Device %s already has a persistent volume."
msgstr " %s උපාංගයට ස්ථාවර වෙළුමක් දැනටමත් පවතී."

#: ../lib/Tails/Persistence/Setup.pm:352
#, perl-format
msgid "Device %s has not enough unallocated space."
msgstr "%s අම්පන්නය සදහා ලබාදී ඇති අවකාශය ප්‍රමාණවත් නොවේ."

#: ../lib/Tails/Persistence/Setup.pm:360 ../lib/Tails/Persistence/Setup.pm:374
#, perl-format
msgid "Device %s has no persistent volume."
msgstr "%s අම්පන්නයෙහි ස්ථාවර වෙළුමක් නොමැත."

#: ../lib/Tails/Persistence/Setup.pm:366
msgid ""
"Cannot delete the persistent volume while in use. You should restart Tails "
"without persistence."
msgstr ""
"ස්ථාවර වෙළුමක් භාවිතා කරමින් ඇති නිසා එය මකා දැමිය නොහැක. ස්ථාවරත්වයක් නොමැතිව Tails නැවත "
"ආරම්භ කර බලන්න."

#: ../lib/Tails/Persistence/Setup.pm:385
msgid "Persistence volume is not unlocked."
msgstr "ස්ථාවර වෙළුම අගුළු හැර නොමැත."

#: ../lib/Tails/Persistence/Setup.pm:390
msgid "Persistence volume is not mounted."
msgstr "ස්ථාවර වෙළුම නංවා නොමැත."

#: ../lib/Tails/Persistence/Setup.pm:395
msgid "Persistence volume is not readable. Permissions or ownership problems?"
msgstr "ස්ථාවර වෙළුම කියවිය නොහැක. අනුමැතිය හෝ අයිතිය පිළිබද ගැටළුවක් ද? "

#: ../lib/Tails/Persistence/Setup.pm:400
msgid "Persistence volume is not writable. Maybe it was mounted read-only?"
msgstr "ස්ථාවර වෙළුමෙහි ලිවිය නොහැක. ඇතැම්විට මෙය කියවීමට පමණක් නංවා ඇත්ද?"

#: ../lib/Tails/Persistence/Setup.pm:409
#, fuzzy, perl-format
msgid "Tails is running from non-USB / non-SDIO device %s."
msgstr "%s USB නොවන අම්පන්නයකින් Tails ක්‍රියාත්මක වේ."

#: ../lib/Tails/Persistence/Setup.pm:415
#, perl-format
msgid "Device %s is optical."
msgstr "%s ප්‍රකාශ උපකරණයකි."

#: ../lib/Tails/Persistence/Setup.pm:422
#, fuzzy, perl-format
msgid "Device %s was not created using Tails Installer."
msgstr "%s අම්පන්නය Tails USB ස්ථාපකය මගින් නිර්මාණය කර නොමැත."

#: ../lib/Tails/Persistence/Setup.pm:668
msgid "Persistence wizard - Finished"
msgstr "ස්ථාවර wizard - අවසානය."

#: ../lib/Tails/Persistence/Setup.pm:671
msgid ""
"Any changes you have made will only take effect after restarting Tails.\n"
"\n"
"You may now close this application."
msgstr ""
"ඔබ සිදු කරන ඕනෑම වෙනස්කමක් Tails නැවත ආරම්භ කලවිට ක්‍රියාත්මක වේ.\n"
"\n"
"ඔබට දැන් මෙම යෙදුම වසා දැමිය හැක."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:54
msgid "Persistence wizard - Persistent volume creation"
msgstr "ස්ථාවර wizard - ස්ථාවර වෙළුම නිර්මාණය කිරීම "

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:57
msgid "Choose a passphrase to protect the persistent volume"
msgstr "ස්ථාවර යෙදුම ආරක්ෂා කරගැනීමට මුරවැකියක් තෝරාගන්න."

#. TRANSLATORS: size, device vendor, device model
#: ../lib/Tails/Persistence/Step/Bootstrap.pm:61
#, perl-format
msgid ""
"A %s persistent volume will be created on the <b>%s %s</b> device. Data on "
"this volume will be stored in an encrypted form protected by a passphrase."
msgstr ""
"%s ස්ථාවර වෙළුමක් <b>%s %s</b> උපාංගයේ නිර්මාණය වනු ඇත. මෙම වෙළුමෙහි දත්ත ගබඩා කර "
"ඇත්තේ සංකේතන කොට මුර වැකියකින් අරක්ෂිතව ය."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:66
msgid "Create"
msgstr "නිර්මාණය කරන්න "

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:110
msgid ""
"<b>Beware!</b> Using persistence has consequences that must be well "
"understood. Tails can't help you if you use it wrong! See <a href='file:///"
"usr/share/doc/tails/website/doc/first_steps/persistence.en.html'>Tails "
"documentation about persistence</a> to learn more."
msgstr ""
"<b>ප්‍රවේසම් වන්න!</b> ස්ථාවර වීම භාවිතයෙන් ඇතිවන පලවිපාක පිළිබද නිසි අවබෝධයක් තිබිය යුතු ය. ඔබ "
"එය වැරදි ලෙස භාවිතා කළහොත් Tails නිසි ලෙස ක්‍රියාත්මක නොවේ! <a href='file:///usr/"
"share/doc/tails/website/doc/first_steps/persistence.en.html'>ස්ථාවරත්වය පිළිබද "
"Tails ප්‍රලේඛන </a> බැලීමෙන් ඔබට වැඩිදුර විස්තර ඉගෙන ගත හැක."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:144
msgid "Passphrase:"
msgstr "මුරවැකිය:"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:154
msgid "Verify Passphrase:"
msgstr "මුරපදය ස්ථිර කරන්න:"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:167
#: ../lib/Tails/Persistence/Step/Bootstrap.pm:231
msgid "Passphrase can't be empty"
msgstr "මුරවැකිය හිස්ව තැබිය නොහැක"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:222
msgid "Passphrases do not match"
msgstr "මුරවැකි ගැලපෙන්නේ නැත "

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:274
#: ../lib/Tails/Persistence/Step/Configure.pm:129
#: ../lib/Tails/Persistence/Step/Delete.pm:95
msgid "Failed"
msgstr "අසමත් විය"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:283
msgid "Mounting Tails persistence partition."
msgstr "Tails ස්ථාවර පද්ධති බෙදුම නංවන්න."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:286
msgid "The Tails persistence partition will be mounted."
msgstr "Tails ස්ථාවර බෙදුම නංවනු ලබනු ඇත."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:295
#, fuzzy
msgid "Correcting permissions of the persistent volume."
msgstr "ස්ථාවර වෙළුම නිර්මාණය කරමින්..."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:298
msgid "The permissions of the persistent volume will be corrected."
msgstr ""

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:320
msgid "Creating..."
msgstr "නිර්මාණය වෙමින් පවතී..."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:323
msgid "Creating the persistent volume..."
msgstr "ස්ථාවර වෙළුම නිර්මාණය කරමින්..."

#: ../lib/Tails/Persistence/Step/Configure.pm:61
msgid "Persistence wizard - Persistent volume configuration"
msgstr "ස්ථාවර wizard - ස්ථාවර වෙළුම වින්‍යාසකරණය "

#: ../lib/Tails/Persistence/Step/Configure.pm:64
msgid "Specify the files that will be saved in the persistent volume"
msgstr "ස්ථාවර වෙළුමෙහි සුරකින ගොනු සදහන් කරන්න."

#. TRANSLATORS: partition, size, device vendor, device model
#: ../lib/Tails/Persistence/Step/Configure.pm:68
#, perl-format
msgid ""
"The selected files will be stored in the encrypted partition %s (%s), on the "
"<b>%s %s</b> device."
msgstr ""
"තෝරාගත් ගොනු සංකේතනය කල %s (%s) බෙදුමෙහි, <b>%s %s</b> උපාංගයේ සුරක්ෂිත කෙරෙනු ඇත."

#: ../lib/Tails/Persistence/Step/Configure.pm:74
msgid "Save"
msgstr "සුරකින්න"

#: ../lib/Tails/Persistence/Step/Configure.pm:143
msgid "Saving..."
msgstr "සුරකිමින්..."

#: ../lib/Tails/Persistence/Step/Configure.pm:146
msgid "Saving persistence configuration..."
msgstr "ස්ථාවර වින්‍යාසකරණය සුරක්ෂිත කරමින්..."

#: ../lib/Tails/Persistence/Step/Delete.pm:41
msgid "Persistence wizard - Persistent volume deletion"
msgstr "ස්ථාවර wizard - ස්ථාවර වෙළුම මකා දැමීම "

#: ../lib/Tails/Persistence/Step/Delete.pm:44
msgid "Your persistent data will be deleted."
msgstr "ඔබේ ස්ථාවර දත්ත මකා දැමෙනු ඇත."

#: ../lib/Tails/Persistence/Step/Delete.pm:48
#, perl-format
msgid ""
"The persistent volume %s (%s), on the <b>%s %s</b> device, will be deleted."
msgstr "<b>%s %s</b> උපාංගයේ පිහිටි ස්ථාවර වෙළුම %s (%s), මකා දැමෙනු ලැබේ."

#: ../lib/Tails/Persistence/Step/Delete.pm:54
msgid "Delete"
msgstr "මකා දමන්න "

#: ../lib/Tails/Persistence/Step/Delete.pm:111
msgid "Deleting..."
msgstr "මකමින්..."

#: ../lib/Tails/Persistence/Step/Delete.pm:114
msgid "Deleting the persistent volume..."
msgstr "ස්ථාවර වෙළුම මකමින්..."

#~ msgid "Claws Mail"
#~ msgstr "Claws තැපෑල "

#~ msgid ""
#~ "The device Tails is running from cannot be found. Maybe you used the "
#~ "`toram' option?"
#~ msgstr ""
#~ "Tails අම්පන්නය ක්‍රියාත්මක වන ස්ථානය සොයාගත නොහැක. ඇතැම් විට ඔබ `toram' විකල්පය "
#~ "භාවිතා කලේද?"
